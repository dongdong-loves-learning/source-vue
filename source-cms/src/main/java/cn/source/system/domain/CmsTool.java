package cn.source.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.source.common.annotation.Excel;
import cn.source.common.core.domain.BaseEntity;

/**
 * 工具管理对象 cms_tool
 * 
 * @author sourcebyte.vip
 * @date 2024-05-24
 */
public class CmsTool extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 工具类型 */
    @Excel(name = "工具类型")
    private String toolType;

    /** 工具名称 */
    @Excel(name = "工具名称")
    private String name;

    /** 工具介绍 */
    private String intro;

    /** 工具缩略图 */
    @Excel(name = "工具缩略图")
    private String thumb;

    /** 工具地址 */
    private String url;

    /** 提取密码 */
    private String extractPassword;

    /** 工具内容 */
    private String content;

    /** 工具版本 */
    @Excel(name = "工具版本")
    private String toolVersion;

    /** 是否热点 */
    @Excel(name = "是否热点")
    private String isHot;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String auditStatus;

    /** 浏览次数 */
    private Long viewCount;

    /** 下载次数 */
    private Long downloadCount;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setToolType(String toolType) 
    {
        this.toolType = toolType;
    }

    public String getToolType() 
    {
        return toolType;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIntro(String intro) 
    {
        this.intro = intro;
    }

    public String getIntro() 
    {
        return intro;
    }
    public void setThumb(String thumb) 
    {
        this.thumb = thumb;
    }

    public String getThumb() 
    {
        return thumb;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setExtractPassword(String extractPassword) 
    {
        this.extractPassword = extractPassword;
    }

    public String getExtractPassword() 
    {
        return extractPassword;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setToolVersion(String toolVersion) 
    {
        this.toolVersion = toolVersion;
    }

    public String getToolVersion() 
    {
        return toolVersion;
    }
    public void setIsHot(String isHot) 
    {
        this.isHot = isHot;
    }

    public String getIsHot() 
    {
        return isHot;
    }
    public void setAuditStatus(String auditStatus) 
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus() 
    {
        return auditStatus;
    }
    public void setViewCount(Long viewCount) 
    {
        this.viewCount = viewCount;
    }

    public Long getViewCount() 
    {
        return viewCount;
    }
    public void setDownloadCount(Long downloadCount) 
    {
        this.downloadCount = downloadCount;
    }

    public Long getDownloadCount() 
    {
        return downloadCount;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("toolType", getToolType())
            .append("name", getName())
            .append("intro", getIntro())
            .append("thumb", getThumb())
            .append("url", getUrl())
            .append("extractPassword", getExtractPassword())
            .append("content", getContent())
            .append("toolVersion", getToolVersion())
            .append("isHot", getIsHot())
            .append("auditStatus", getAuditStatus())
            .append("viewCount", getViewCount())
            .append("downloadCount", getDownloadCount())
            .append("sort", getSort())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .toString();
    }
}
